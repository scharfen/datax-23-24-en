# -*- coding: utf-8 -*-
import nltk
import numpy as np
import pandas as pd
import seaborn as sns
import sweetviz as sv
import matplotlib.pyplot as plt
from wordcloud import WordCloud
from nltk.corpus import stopwords
from matplotlib.dates import DateFormatter
# Download the stopwords from nltk
nltk.download('stopwords')


def generate_sweetviz_report(data, name):
    report = sv.analyze(data)
    report.show_html(name)
    
    
def load_data_pre(path2de, path2en):
    de = load_data(path2de)
    en = load_data(path2en)
    de['lang'] = 'de'
    en['lang'] = 'en'
    df = pd.concat([de, en], ignore_index=True)
    df['Q01_[SD1] Gender'] = df['Q01_[SD1] Gender'].replace({'2 : männlich': '2 : male',
                                                         '1 : weiblich': '1 : female',
                                                         '3 : divers': '3 : diverse',
                                                         '4 : keine Angabe': '4 : not specified'})
    df['Q09_[SD9] Residence'] = df['Q09_[SD9] Residence'].replace({'1 : Stadt': '1 : City',
                                                               '2 : Vorort': '2 : Suburb',
                                                               '3 : Land': '3 : Country',
                                                               '4 : Anderes': '4 : Other'})
    return df


def load_data(path2file):
    return pd.read_csv(path2file)


def create_wordcloud_languages(data, lang='de'):
    
    subset_data = data[data['lang']==lang].copy()
    text = ' '.join(subset_data['Q04_[SD4] Native language'].dropna().tolist())
    text = text.lower()

    # Remove german
    for k, v in {'Deutsch': '', 'Deutschland': '', 'Deustch': '', 'deutsch': '', 'und': '', 'land': ''}.items():
        text = text.replace(k.lower(), v)

    # Generate the word cloud
    wordcloud = WordCloud(width=800, height=400, background_color='white').generate(text)

    # Plot the word cloud
    plt.figure(figsize=(10, 6))
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis('off')
    if lang=='de':
        plt.title('Native Languages in German moodle course (not considering German)')
    else:
        plt.title('Native Languages in English moodle course')
    plt.show()


def plot_top_n_majors(data, n=10):
    # Get the top 20 most frequent values from the column
    top_n_values = data['Q13_[SC1] Study Choice Major'].value_counts().head(n)
    # Shorten long labels
    top_n_values = top_n_values.rename({'International Business Administration & Entrepreneurship': 'IBAE',
                                        'Global Environmental & Sustainability Studies': 'GESS'})
    # Create a bar chart
    plt.figure(figsize=(10, 6))  # Set the figure size
    sns.barplot(y=top_n_values.index, x=top_n_values.values)
    #plt.xticks(rotation=90)
    plt.ylabel('Majors')
    plt.xlabel('Count')
    plt.title(f'Top {min(n, len(top_n_values))} Majors')
    plt.show()


def plot_schools(data):
    # Get the top 10 most frequent values from the column
    studs_per_dep = data['Q13_[SC1b] Fakultaet'].value_counts()

    # Create a bar chart
    plt.figure(figsize=(10, 6))  # Set the figure size
    sns.barplot(x=studs_per_dep.index, y=studs_per_dep.values)
    plt.xticks(rotation=90)
    plt.xlabel('schools')
    plt.ylabel('Count')
    plt.title('Number of freshman per school')
    plt.show()


def plot_age_distribution(data):
    # Plotting the histogram
    sns.histplot(data['Q02_[SD2] Age'], bins=30, kde=True, color='blue')
    plt.title('Age Distribution')
    plt.xlabel('Age')
    plt.ylabel('Frequency')
    plt.show()


def plot_age_per_school(data):
    plt.figure(figsize=(10, 6))  # Set the figure size
    sns.boxplot(data=data, x='Q13_[SC1b] Fakultaet', y='Q02_[SD2] Age')

    plt.xlabel('school')
    plt.ylabel('Age')
    plt.title('Boxplot of Age per school')
    plt.xticks(rotation=45)
    plt.show()


def plot_gender_per_school(data):
    relative_frequencies = data.groupby(['Q01_[SD1] Gender',
                                         'Q13_[SC1b] Fakultaet']).size() / data.groupby(['Q13_[SC1b] Fakultaet']).size()
    relative_frequencies = relative_frequencies.reset_index(name='relative_frequency')

    # Create a side-by-side bar chart
    plt.figure(figsize=(10, 6))  # Set the figure size
    sns.barplot(data=relative_frequencies, x='Q13_[SC1b] Fakultaet', y='relative_frequency', hue='Q01_[SD1] Gender', errorbar=None)

    plt.xlabel('Fakultät')
    plt.ylabel('Relative frequency')
    plt.title('Side-by-Side Bar Chart of gender vs school')
    plt.legend(title='Categories')

    plt.xticks(rotation=45)
    plt.show()


def plot_submission_timeline(data, per_school=False, relative=False):
    # Convert the 'date_column' to datetime (if not already done)
    data['Abgegeben:'] = pd.to_datetime(data['Abgegeben:'], format='%Y-%m-%d %H:%M:%S')

    # Sort the DataFrame by the datetime column
    data = data.sort_values(by='Abgegeben:')

    # Create a new column representing the cumulative count
    data['cumulative_count'] = range(1, len(data) + 1)
    
    # Create a line chart for each residence category
    plt.figure(figsize=(10, 6))
    
    if per_school:
        residence_categories = data['Q13_[SC1b] Fakultaet'].unique()

        for category in residence_categories:
            subset_df = data[data['Q13_[SC1b] Fakultaet'] == category].copy()  # Use .copy() to avoid warnings
            subset_df['cumulative_count'] = range(1, len(subset_df) + 1)
            if relative:
                subset_df['cumulative_count'] = subset_df['cumulative_count'] / len(subset_df)
            plt.plot(subset_df['Abgegeben:'].to_numpy(), subset_df['cumulative_count'].to_numpy(), marker='o', linestyle='-', label=category)
            plt.legend(loc='upper left')
    else:
        plt.plot(data['Abgegeben:'].to_numpy(), data['cumulative_count'].to_numpy(), marker='o', linestyle='-')

    plt.xlabel('Timestamp')
    if relative:
        plt.ylabel('Percentage')
    else:
        plt.ylabel('Number of Submissions')
    plt.title('Submissions Over Time')
    plt.grid(True)

    # Format the x-axis date labels
    date_format = DateFormatter('%d.%m. %H:%M')
    plt.gca().xaxis.set_major_formatter(date_format)

    plt.xticks(rotation=45)
    plt.tight_layout()
    plt.show()
    
    
def plot_submission_timeline_course(data, per_lang=False, relative=False):
    # Convert the 'date_column' to datetime (if not already done)
    data['Abgegeben:'] = pd.to_datetime(data['Abgegeben:'], format='%Y-%m-%d %H:%M:%S')

    # Sort the DataFrame by the datetime column
    data = data.sort_values(by='Abgegeben:')

    # Create a new column representing the cumulative count
    data['cumulative_count'] = range(1, len(data) + 1)
    
    # Create a line chart for each residence category
    plt.figure(figsize=(10, 6))
    
    if per_lang:
        courses = data['lang'].unique()

        for category in courses:
            subset_df = data[data['lang'] == category].copy()  # Use .copy() to avoid warnings
            subset_df['cumulative_count'] = range(1, len(subset_df) + 1)
            if relative:
                subset_df['cumulative_count'] = subset_df['cumulative_count'] / len(subset_df)
            plt.plot(subset_df['Abgegeben:'].to_numpy(), subset_df['cumulative_count'].to_numpy(), marker='o', linestyle='-', label=category)
            plt.legend(loc='upper left')
    else:
        plt.plot(data['Abgegeben:'].to_numpy(), data['cumulative_count'].to_numpy(), marker='o', linestyle='-')

    plt.xlabel('Timestamp')
    plt.ylabel('Percentage')
    plt.title('Submissions Over Time')
    plt.grid(True)

    # Format the x-axis date labels
    date_format = DateFormatter('%d.%m. %H:%M')
    plt.gca().xaxis.set_major_formatter(date_format)

    plt.xticks(rotation=45)
    plt.tight_layout()
    plt.show()


def plot_wordcloud(data, column_name, per_school=False, lang='de'):
    '''
    column_name can take these values:'Q16_[SC4] Reasoning Study Choice', 
            'Q17_[SC5] Expectations', 
            'Q18_[SC6] Learning-Expectations'
    '''
    
    subset_data = data[data['lang']==lang].copy()
    
    if lang=='de':
        german_stopwords = set(stopwords.words('german'))
        german_stopwords.update(set(stopwords.words('english')))
        german_stopwords.update(['Leuphana', 'Studium', 'Uni', 'Universität', 'studieren', 'Außerdem'])

    # Assuming you already have a dataframe named df
    if per_school:
        for dep in subset_data['Q13_[SC1b] Fakultaet'].unique():
            dep_data = subset_data[subset_data['Q13_[SC1b] Fakultaet'] == dep]
            text = ' '.join(dep_data[column_name].dropna().tolist())
            # Generate the word cloud excluding German stopwords
            wordcloud = WordCloud(width=800, 
                                  height=400, 
                                  background_color='white', 
                                  stopwords=german_stopwords).generate(text)
            # Plot the word cloud
            plt.figure(figsize=(10, 6))
            plt.imshow(wordcloud, interpolation='bilinear')
            plt.axis('off')
            plt.title(f'Word Cloud for {column_name} at {dep}')
            plt.show()
    else:
        text = ' '.join(subset_data[column_name].dropna().tolist())
        # Generate the word cloud excluding German stopwords
        wordcloud = WordCloud(width=800, 
                              height=400, 
                              background_color='white', 
                              stopwords=german_stopwords).generate(text)
        # Plot the word cloud
        plt.figure(figsize=(10, 6))
        plt.imshow(wordcloud, interpolation='bilinear')
        plt.axis('off')
        plt.title(f'Word Cloud for {column_name}')
        plt.show()


def plot_correlation_matrix(data):
    df_encoded = pd.get_dummies(data, columns=['Q13_[SC1b] Fakultaet', 'Q08_[SD8] Income level',
                                               'Q01_[SD1] Gender', 'Q09_[SD9] Residence'])

    correlation_matrix = df_encoded.corr(numeric_only=True)

    # If you want to visualize the correlation matrix using a heatmap:
    plt.figure(figsize=(24, 24))
    sns.heatmap(correlation_matrix, annot=True, cmap='coolwarm', vmin=-1, vmax=1)
    plt.title("Correlation Matrix")
    plt.show()



def plot_income_per_school(data):
    relative_frequencies = data.groupby(['Q08_[SD8] Income level',
                                         'Q13_[SC1b] Fakultaet']).size() / data.groupby(['Q13_[SC1b] Fakultaet']).size()
    relative_frequencies = relative_frequencies.reset_index(name='relative_frequency')

    # Create a side-by-side bar chart
    plt.figure(figsize=(10, 6))  # Set the figure size

    # Use a green-based palette
    #palette = sns.light_palette("darkgreen", reverse=False, n_colors=6)
    palette = sns.diverging_palette(10, 133, n=6)
    sns.barplot(data=relative_frequencies, x='Q13_[SC1b] Fakultaet', y='relative_frequency', hue='Q08_[SD8] Income level', errorbar=None, palette=palette)

    plt.xlabel('Fakultät')
    plt.ylabel('Relative frequency')
    plt.title('Side-by-Side Bar Chart of income vs school')
    plt.legend(title='Categories', loc='upper center')

    plt.xticks(rotation=45)
    plt.show()



def plot_text_lengths(data):
    cols = ['Q16_[SC4] Reasoning Study Choice', 
            'Q17_[SC5] Expectations', 
            'Q18_[SC6] Learning-Expectations']


    for col in cols:
        # Calculate the string length for each value in the column
        length = data[col].str.len()
        # Print statistics
        print(f"Column: {col}")
        print(f"Minimum text length:", length.min())
        print(f"Mean text length:", round(length.mean(), 2))
        print(f"Standard deviation:", round(length.std(), 2))
        print(f"Median text length:", length.median())
        print(f"Maximum text length:", length.max())
        # Plot the histogram
        plt.figure(figsize=(10, 6))
        plt.hist(length.dropna(), bins=75, edgecolor='black')  # dropna() to exclude NaN values
        plt.axvline(length.mean(), color='red', linestyle='dashed', linewidth=2, label='Mean')  # Draw a vertical line at the mean
        plt.axvline(length.median(), color='green', linestyle='dashed', linewidth=2, label='Median')  # Draw a vertical line at the mean
        plt.title(f'Histogram of String Lengths for {col}')
        plt.xlabel('String Length')
        plt.ylabel('Frequency')
        plt.legend(loc='upper right')
        plt.grid(True, which='both', linestyle='--', linewidth=0.5)
        plt.show()


def plot_text_length_per_gender(data):
    cols = ['Q16_[SC4] Reasoning Study Choice', 
            'Q17_[SC5] Expectations', 
            'Q18_[SC6] Learning-Expectations']
    # Get unique categories from 'Q13_[SC1b] Fakultaet'
    categories = data['Q01_[SD1] Gender'].unique()

    for col in cols:
        data[f'len_{col}'] = data[col].str.len()
        plt.figure(figsize=(10, 6))

        # Create a boxplot for each category
        sns.boxplot(x='Q01_[SD1] Gender', y=f'len_{col}', data=data, order=categories)

        plt.title(f'Boxplot of Text Lengths for "{col}"')
        plt.ylabel('Text Length')
        plt.xlabel('school')
        plt.xticks(rotation=45)
        plt.grid(True, which='both', linestyle='--', linewidth=0.5, axis='y')
        plt.tight_layout()
        plt.show()
