# -*- coding: utf-8 -*-
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


def load_data_pre(path2de, path2en):
    de = load_data(path2de)
    en = load_data(path2en)
    de['lang'] = 'de'
    en['lang'] = 'en'
    df = pd.concat([de, en], ignore_index=True)
    df['Q01_[SD1] Gender'] = df['Q01_[SD1] Gender'].replace({'2 : männlich': '2 : male',
                                                         '1 : weiblich': '1 : female',
                                                         '3 : divers': '3 : diverse',
                                                         '4 : keine Angabe': '4 : not specified'})
    df['Q09_[SD9] Residence'] = df['Q09_[SD9] Residence'].replace({'1 : Stadt': '1 : City',
                                                               '2 : Vorort': '2 : Suburb',
                                                               '3 : Land': '3 : Country',
                                                               '4 : Anderes': '4 : Other'})
    return df


def load_data(path2file):
    return pd.read_csv(path2file)


def plot_residence(data):
    custom_palette = {
        "1 : City": "#3498db",
        "2 : Suburb": "#f39c12",
        "3 : Country": "#2ecc71",
        "4 : Other": "#e74c3c"
    }
    bar_order = ["1 : City", "2 : Suburb", "3 : Country", "4 : Other"]
    sns.set_style("whitegrid")
    # Visualization of absolute values for 'Q09_[SD9] Residence'
    plt.figure(figsize=(10, 6))
    sns.countplot(data=data, x='Q09_[SD9] Residence', palette=custom_palette, order=bar_order)
    plt.title('Absolute Counts of Residence')
    plt.xlabel('Residence')
    plt.ylabel('Count')
    plt.show()


def plot_school_relative(data):
    relative_values = data['Q13_[SC1b] Fakultaet'].value_counts(normalize=True) * 100
    plt.figure(figsize=(10, 6))
    sns.barplot(x=relative_values.index, y=relative_values.values)
    plt.title('Freshmen per school')
    plt.xlabel('School')
    plt.ylabel('Percentage (%)')
    plt.xticks(rotation=45)
    plt.show()

def plot_residence_per_school(data):
    custom_palette = {
        "1 : City": "#3498db",
        "2 : Suburb": "#f39c12",
        "3 : Country": "#2ecc71",
        "4 : Other": "#e74c3c"
    }
    hue_order = ["1 : City", "2 : Suburb", "3 : Country", "4 : Other"]
    grouped = data.groupby('Q13_[SC1b] Fakultaet')['Q09_[SD9] Residence'].value_counts(normalize=True).reset_index(name='Percentage')
    grouped['Percentage'] *= 100
    plt.figure(figsize=(12, 6))
    sns.barplot(x='Q13_[SC1b] Fakultaet', y='Percentage', hue='Q09_[SD9] Residence', data=grouped, palette=custom_palette, hue_order=hue_order)
    plt.title('Percentages of residence per school')
    plt.xlabel('School')
    plt.ylabel('Percentage (%)')
    plt.xticks(rotation=45)
    plt.legend(title='', bbox_to_anchor=(1.05, 1), loc='upper left')
    plt.tight_layout()
    plt.show()

'''
def plot_residence_per_school(data):
    grouped = data.groupby('Q13_[SC1b] Fakultaet')['Q09_[SD9] Residence'].value_counts(normalize=True).unstack() * 100
    grouped.plot(kind='bar', stacked=False, figsize=(12, 6))
    plt.title('Percentages of residence per school')
    plt.xlabel('School')
    plt.ylabel('Percentage (%)')
    plt.xticks(rotation=45)
    plt.show()
'''