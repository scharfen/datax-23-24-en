# Updating the Hangman game code to overwrite lines in Jupyter Notebook using clear_output

from IPython.display import clear_output
import random
import time

def choose_word():
    words = ['python', 'jupyter', 'data', 'analysis', 'notebook']
    return random.choice(words)


def display_hangman(tries):
    stages = [  # final state: head, torso, both arms, and both legs
                """
                   --------
                   |      |
                   |      O
                   |     \\|/
                   |      |
                   |     / \\
                   -
                """,
                # head, torso, both arms, and one leg
                """
                   --------
                   |      |
                   |      O
                   |     \\|/
                   |      |
                   |     / 
                   -
                """,
                # head, torso, and both arms
                """
                   --------
                   |      |
                   |      O
                   |     \\|/
                   |      |
                   |      
                   -
                """,
                # head, torso, and one arm
                """
                   --------
                   |      |
                   |      O
                   |     \\|
                   |      |
                   |     
                   -
                """,
                # head and torso
                """
                   --------
                   |      |
                   |      O
                   |      |
                   |      |
                   |     
                   -
                """,
                # head
                """
                   --------
                   |      |
                   |      O
                   |    
                   |      
                   |     
                   -
                """,
                # initial empty state
                """
                   --------
                   |      |
                   |      
                   |    
                   |      
                   |     
                   -
                """
    ]
    return stages[tries]

def play_hangman():
    word = choose_word()
    word_completion = '_ ' * len(word)
    guessed = False
    guessed_letters = []
    guessed_words = []
    tries = 6
    print("Let's play Hangman!")
    
    while not guessed and tries > 0:
        clear_output(wait=False)
        print(display_hangman(tries))
        print(word_completion + '\n')
        print('Previous guesses: ' + ' '.join(guessed_letters))
        
        guess = input('Please enter a letter or word: ')
        guess = guess.lower()
        
        if len(guess) == 1 and guess.isalpha():
            if guess in guessed_letters:
                print("You already guessed the letter", guess)
            elif guess not in word:
                print(guess, "is not in the word.")
                tries -= 1
                guessed_letters.append(guess)
            else:
                print("Good job,", guess, "is in the word!")
                guessed_letters.append(guess)
                word_as_list = list(word_completion)
                indices = [i for i, letter in enumerate(word) if letter == guess]
                for index in indices:
                    #word_as_list[index] = guess
                    word_as_list[index * 2] = guess  
                word_completion = "".join(word_as_list)
                if "_" not in word_completion:
                    guessed = True
        elif len(guess) == len(word) and guess.isalpha():
            if guess in guessed_words:
                print("You already guessed the word", guess)
            elif guess != word:
                print(guess, "is not the word.")
                tries -= 1
                guessed_words.append(guess)
            else:
                guessed = True
                #word_completion = word
                word_completion = ' '.join(word)
        else:
            print("Not a valid guess.")
        time.sleep(2)
    
    clear_output(wait=False)
    if guessed:
        print("Congrats, you guessed the word! You win!")
        print(display_hangman(tries))
        print(word)
    else:
        print("Sorry, you ran out of tries. The word was " + word + ". Maybe next time!")
        print(display_hangman(tries))

        
play_hangman()